package Ingredientes;


import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.scorp.splashscreen.R;

import navigation.MainActivityS;


/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class IngredientsFragment extends Fragment implements AdapterView.OnItemClickListener {
    private GridView gv;
    static TypedArray icons;
    static String[] nombres = {""};
    Resources res;
    String tipo = "";

    public IngredientsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_ingredients, container, false);

        //Recuperamos el tipo de ingrediente que cargará.
        tipo = getArguments().getString("tipo");

        res = getResources();
        selection(tipo);
        gv = (GridView) v.findViewById(R.id.gridview);
        gv.setAdapter(new ImageAdapter(getActivity()));
        gv.setOnItemClickListener(this);
        return v;
    }

    //metodo para seleccionar grupo de ingredientes dependiendo del boton clickado
    public void selection(String result) {
        if (result.equals("1")) {
            //Texto del titulo del toolbar y el subtitulo.
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.leche));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.leche2));
            nombres = getResources().getStringArray(R.array.nombresLeche);
            icons = res.obtainTypedArray(R.array.imagenesLeche);
        } else if (result.equals("2")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.carne));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
            nombres = getResources().getStringArray(R.array.nombresCarne);
            icons = res.obtainTypedArray(R.array.imagenesCarnes);
        } else if (result.equals("3")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.pescado));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
            nombres = getResources().getStringArray(R.array.nombresPescado);
            icons = res.obtainTypedArray(R.array.imagenesPescado);
        } else if (result.equals("4")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.patatas));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.patatas2));
            nombres = getResources().getStringArray(R.array.nombresLegumbres);
            icons = res.obtainTypedArray(R.array.imagenesLegumbres);
        } else if (result.equals("5")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.verduras));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
            nombres = getResources().getStringArray(R.array.nombresVerduras);
            icons = res.obtainTypedArray(R.array.imagenesVerduras);
        } else if (result.equals("6")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.fruta));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
            nombres = getResources().getStringArray(R.array.nombresFrutas);
            icons = res.obtainTypedArray(R.array.imagenesFrutas);
        } else if (result.equals("7")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.pan));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.pan2));
            nombres = getResources().getStringArray(R.array.nombresCereales);
            icons = res.obtainTypedArray(R.array.imagenesCereales);
        } else if (result.equals("8")) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.aceite));
            ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(getString(R.string.aceite2));
            nombres = getResources().getStringArray(R.array.nombresGrasas);
            icons = res.obtainTypedArray(R.array.imagenesGrasas);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String nameIng = (String) parent.getItemAtPosition(position);
        android.support.v4.app.FragmentManager fm = getFragmentManager();
        DialogFragment1 dialogFragment = new DialogFragment1();
        Bundle args = new Bundle();
        args.putString("name", nameIng);
        dialogFragment.setArguments(args);
        dialogFragment.show(fm, "Sample Fragment");
    }
}


