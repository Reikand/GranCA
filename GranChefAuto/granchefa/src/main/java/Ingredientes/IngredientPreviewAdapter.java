package Ingredientes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.scorp.splashscreen.R;

import java.util.List;

import POJO.Ingredients;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

public class IngredientPreviewAdapter extends BaseAdapter {
    private List<Ingredients> ingrediente;
    private LayoutInflater layoutinflater;

    public IngredientPreviewAdapter(Context context, List<Ingredients> listaFavoritos) {
        this.ingrediente = listaFavoritos;
        this.layoutinflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return ingrediente.size();
    }

    @Override
    public Object getItem(int position) {
        return ingrediente.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes que representen
    static class ViewHolder {
        TextView nombreIngrediente;
        TextView cantidadIngrediente;
        ImageButton btncancel;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutinflater.inflate(R.layout.item_preview_ingredients, null);
            holder = new ViewHolder();
            holder.nombreIngrediente = (TextView) convertView.findViewById(R.id.txtIngredientes);
            holder.cantidadIngrediente = (TextView) convertView.findViewById(R.id.txtCantidadIngredientes);
            holder.btncancel = (ImageButton) convertView.findViewById(R.id.btnCancel);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.nombreIngrediente.setText(" " + ingrediente.get(position).getNombre());
        holder.cantidadIngrediente.setText(" " + ingrediente.get(position).getCantidad());
        holder.btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingrediente.remove(position);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}