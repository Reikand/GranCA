package Ingredientes;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.scorp.splashscreen.R;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

public class ImageAdapter extends BaseAdapter {
    private Context context;


    public ImageAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return (IngredientsFragment.nombres).length;
    }

    @Override
    public Object getItem(int position) {
        return (IngredientsFragment.nombres[position]);
    }

    //revisar
    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.gridview_item, parent, false);
        }
        ImageView iv = (ImageView) view.findViewById(R.id.imagen);
        TextView tv = (TextView) view.findViewById(R.id.texto);
        Drawable drawable = IngredientsFragment.icons.getDrawable(position);
        iv.setImageDrawable(drawable);
        tv.setText(IngredientsFragment.nombres[position]);

        return view;
    }
}


