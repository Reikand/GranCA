package Ingredientes;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;

import ListaIngredientes.ListaIngredientes_Fragment;


/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class DialogFragment1 extends DialogFragment implements View.OnClickListener {


    Button accept;
    EditText et;
    private Spinner sp;
    boolean valid = true;

    public DialogFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dialog, container, false);
        et = (EditText) v.findViewById(R.id.etCantidad);
        sp = (Spinner) v.findViewById(R.id.spinner);
//        sp.setOnItemSelectedListener(this);

        accept = (Button) v.findViewById(R.id.dismiss);
        accept.setOnClickListener(this);

        getDialog().setTitle("Simple dialog");

        ArrayAdapter<CharSequence> aa = ArrayAdapter.createFromResource(getActivity(), R.array.unidades, android.R.layout.simple_list_item_1);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(aa);

        return v;
    }

    /**
     * Muestra un dialog para indicar la cantidad de ingredientes y el tipo
     * además hace que siempre aparezca el teclado numerico al abrirlo.
     *
     * @param savedInstanceState
     * @return
     */

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        //Para no mostrar titulo.
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        return dialog;
    }


    @Override
    public void onClick(View v) {
//

        String nameIng = getArguments().getString("name");

        //Cogemos el valor indicado por el usuario en el dialog.

        int cantidad = Integer.parseInt(et.getText().toString());

        //Comprueba que no está repetido el ingrediente.
        if (ListaIngredientes_Fragment.containsIngredient(nameIng)) {
            Toast.makeText(getActivity(), "Ingrediente " + nameIng + " ya añadido", Toast.LENGTH_SHORT).show();
        } else {
                ListaIngredientes_Fragment.addIngredient(nameIng, cantidad);
                Toast.makeText(getActivity(), "Ingrediente " + nameIng + " añadido a la lista", Toast.LENGTH_SHORT).show();
        }
        dismiss();

    }


}
