package Asyntask;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import POJO.Usuario;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class AsyncTaskLogin extends AsyncTask<String, Void, String> {

    Context context;

    public AsyncTaskLogin(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... args) {

        RequestServlet request = new RequestServlet();

        String result = request.get(args[0]);

        return comprobarResultado(result);
    }

    public String comprobarResultado(String resultado) {

        String objectJson = new String();

        try {

//            String.valueOf(new JSONObject(resultado).get("codigo"));
            String cod = String.valueOf(new JSONObject(resultado).get("codigo"));

            if (cod.equals("200")){
                objectJson = String.valueOf(new JSONObject(resultado).get("usuario"));
                return objectJson;
            } else if (cod.equals("400")){
                return "-1";
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "-1";
    }
}
