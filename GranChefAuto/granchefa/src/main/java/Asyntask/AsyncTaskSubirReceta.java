package Asyntask;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class AsyncTaskSubirReceta extends AsyncTask<String, Void, String> {

    Context context;

    public AsyncTaskSubirReceta(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... args) {

        RequestservletSubirBuscar request = new RequestservletSubirBuscar();

        String result = request.get(args[0], args[1], args[2]);

        return comprobarResultado(result);
    }

    public String comprobarResultado(String resultado) {

        try {
            String cod = String.valueOf(new JSONObject(resultado).get("codigo"));
            if (cod.equals("200")){
                return "1";
            } else if (cod.equals("400")) {
                return "-1";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "-1";
    }

}
