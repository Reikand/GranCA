package Asyntask;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class AsyncTaskBuscarReceta extends AsyncTask<String, Void, String> {

    private Context context;

    public AsyncTaskBuscarReceta(Context context) {
        this.context = context;
    }


    @Override
    protected String doInBackground(String... args) {

        RequestservletSubirBuscar request = new RequestservletSubirBuscar();

        String result = request.get(args[0], args[1], args[2]);

        return comprobarResultado(result);
    }

    //Metodo aun no funcional
    public String comprobarResultado(String resultado) {

        String objectJson = "";

        try {
            objectJson = String.valueOf(new JSONObject(resultado).get("lista_recetas"));
            System.out.println(objectJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return objectJson;
    }

}
