package Asyntask;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import POJO.Usuario;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class AsyncTaskRegistro extends AsyncTask<String, Void, String> {

    private Context context;

    public AsyncTaskRegistro(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... args) {

        RequestServlet request = new RequestServlet();

        String result = request.get(args[0]);

        return comprobarResultado(result);
    }

    public String comprobarResultado(String resultado) {

        try {

            String cod = String.valueOf(new JSONObject(resultado).get("codigo"));

            if (cod.equals("200")) {

                String usuario = String.valueOf(new JSONObject(resultado).get("usuario"));
                return usuario;
            } else if (cod.equals("400")){
                return "-1";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "-1";
    }
}
