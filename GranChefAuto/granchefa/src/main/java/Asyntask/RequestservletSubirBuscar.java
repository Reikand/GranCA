package Asyntask;

import android.support.annotation.Nullable;
import android.util.JsonReader;

import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class RequestservletSubirBuscar {

//    HttpURLConnection urlConnection = null;

    public String get(String direccion, String param1, String param2){

//        InputStream is = null;
        String result = "";

        try {
            String valor = URLEncoder.encode(param2, "UTF-8");

            URL url = new URL(direccion + param1 + valor);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            //Enviamos la peticion al servidor.
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setConnectTimeout(8000);
            urlConnection.setFixedLengthStreamingMode(0);
            urlConnection.connect();

            OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
            out.flush();
            out.close();

            //Recuperamos el mensaje que nos envia el servidor para luego tratarlo.
//            int statusCode = urlConnection.getResponseCode();

            InputStream is = new BufferedInputStream(urlConnection.getInputStream());

            result = getInputStreamtoString(is);

            is.close();
            urlConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    public String getInputStreamtoString(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
