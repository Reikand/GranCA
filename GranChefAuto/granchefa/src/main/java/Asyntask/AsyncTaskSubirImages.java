package Asyntask;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Base64;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import subirrecetas.SubirRecetasFotos;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class AsyncTaskSubirImages extends AsyncTask<String, Void, String> {

    Context context;

    public AsyncTaskSubirImages(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {

        JSONObject jsonObject = new JSONObject();
        List<Bitmap> listaFotos = SubirRecetasFotos.returnListFoto();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        List<String> listImageEncoded = new ArrayList<String>();

        for (Bitmap bitmap : listaFotos) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();
            String imgEncoded = Base64.encodeToString(data, Base64.DEFAULT);
            listImageEncoded.add(imgEncoded);
        }
        //Transformamos ya la lista a Gson para poder pasarla ya directamente a la peticion.
        try {
            jsonObject.put("listafotos", listImageEncoded);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();
    }
}
