package pantallastutorialandsplash;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.LinearLayout;

import com.example.scorp.splashscreen.R;

import pantallastutorialandsplash.Pantallatuto1;
import pantallastutorialandsplash.Pantallatuto2;
import pantallastutorialandsplash.Pantallatuto3;
import pantallastutorialandsplash.Pantallatuto4;
import pantallastutorialandsplash.Pantallatuto5;
import pantallastutorialandsplash.Pantallatuto6;
import pantallastutorialandsplash.Pantallatuto7;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */


public class ImageViewPagerAdapter extends FragmentPagerAdapter {
    private Context _context;
    public static int totalPage = 7;

    public ImageViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context = context;

    }



    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch (position) {
            case 0:
                f = new Pantallatuto1();
                break;
            case 1:
                f = new Pantallatuto2();
                break;
            case 2:
                f = new Pantallatuto3();
                break;
            case 3:
                f = new Pantallatuto4();
                break;
            case 4:
                f = new Pantallatuto5();
                break;
            case 5:
                f = new Pantallatuto6();
                break;
            case 6:
                f = new Pantallatuto7();
                break;
        }
        return f;
    }

    @Override
    public int getCount() {

        return totalPage;
    }

}

