package pantallastutorialandsplash;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.scorp.splashscreen.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private ViewPager mViewPager;
    private ImageViewPagerAdapter adapter;
    private ImageView btn1, btn2, btn3,btn4,btn5,btn6,btn7;
    public MainActivityFragment() {
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpView();
        setTab();
        onCircleButtonClick();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    private void onCircleButtonClick() {

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn1.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(0);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn2.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(1);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn3.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(2);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn4.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(3);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn5.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(4);
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn6.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(5);
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn7.setImageResource(R.drawable.ic_lens);
                mViewPager.setCurrentItem(6);
            }
        });
    }

    private void setUpView() {
        mViewPager = (ViewPager) getView().findViewById(R.id.imageviewPager);
        adapter = new ImageViewPagerAdapter(getActivity(), getFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setCurrentItem(0);
        initButton();
    }

    private void setTab() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageSelected(int position) {

                btn1.setImageResource(R.drawable.ic_white);
                btn2.setImageResource(R.drawable.ic_white);
                btn3.setImageResource(R.drawable.ic_white);
                btn4.setImageResource(R.drawable.ic_white);
                btn5.setImageResource(R.drawable.ic_white);
                btn6.setImageResource(R.drawable.ic_white);
                btn7.setImageResource(R.drawable.ic_white);
                btnAction(position);
            }

        });

    }

    private void btnAction(int action) {
        switch (action) {
            case 0:
                btn1.setImageResource(R.drawable.ic_lens);
                break;
            case 1:
                btn2.setImageResource(R.drawable.ic_lens);
                break;
            case 2:
                btn3.setImageResource(R.drawable.ic_lens);
                break;
            case 3:
                btn4.setImageResource(R.drawable.ic_lens);
                break;
            case 4:
                btn5.setImageResource(R.drawable.ic_lens);
                break;
            case 5:
                btn6.setImageResource(R.drawable.ic_lens);
                break;
            case 6:
                btn7.setImageResource(R.drawable.ic_lens);
                break;
        }
    }

    private void initButton() {
        btn1 = (ImageView) getView().findViewById(R.id.btn1);
        btn1.setImageResource(R.drawable.ic_lens);
        btn2 = (ImageView) getView().findViewById(R.id.btn2);
        btn3 = (ImageView) getView().findViewById(R.id.btn3);
        btn4 = (ImageView) getView().findViewById(R.id.btn4);
        btn5 = (ImageView) getView().findViewById(R.id.btn5);
        btn6 = (ImageView) getView().findViewById(R.id.btn6);
        btn7 = (ImageView) getView().findViewById(R.id.btn7);

    }
}
