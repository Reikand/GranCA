package pantallastutorialandsplash;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import Asyntask.AsyncTaskLogin;
import Constantes.Constantes;
import POJO.Invitado;
import POJO.Usuario;
import butterknife.InjectView;
import loginsingup.SignupActivity;
import navigation.MainActivityS;
import resultadolistarecetas.Productselec;


/**
 * A simple {@link Fragment} subclass.
 */
public class Pantallatuto7 extends Fragment implements View.OnClickListener {

    private static final String TAG = "LoginActivity";

    //Componentes de la pantalla de login inicial
    private TextView registro;
    private TextView invitado;
    private Context context;
    private EditText loginEmail, loginPass;
    private CheckBox checkSession;
    private Button loginButton;
    private ProgressDialog progressDialog;

    private Usuario userLogin;
    String res = "";

    public Pantallatuto7() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v7 = inflater.inflate(R.layout.activity_login, container, false);
        context = v7.getContext();

        //Instanciamos los componentes del layout.
        loginEmail = (EditText) v7.findViewById(R.id.input_email);
        loginPass = (EditText)v7.findViewById(R.id.input_password);

        checkSession = (CheckBox)v7.findViewById(R.id.checkbox);

        loginButton = (Button) v7.findViewById(R.id.btn_login);
        loginButton.setOnClickListener(this);

        registro = (TextView) v7.findViewById(R.id.link_signup);
        registro.setOnClickListener(this);

        invitado = (TextView) v7.findViewById(R.id.link_invitado);
        invitado.setOnClickListener(this);

        return v7;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.link_signup) {
            Intent intent = new Intent(getActivity(), SignupActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.link_invitado) {
            Invitado inv = new Invitado("Invitado");
            Intent intent = new Intent(getActivity(), MainActivityS.class);
            intent.putExtra("Usuario", inv);
            startActivity(intent);
        } else if (v.getId() == R.id.btn_login) {

            login();

        }

    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        loginButton.setEnabled(true);

        progressDialog = new ProgressDialog(getActivity(),
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autentificando...");
        progressDialog.show();


        String email = loginEmail.getText().toString();
        String password = loginPass.getText().toString();

        String url = Constantes.URL_LOGIN + "&email=" + email + "&pass=" + password;

        AsyncTaskLogin asyncLogin = new AsyncTaskLogin(getActivity());

        asyncLogin.execute(url);

        try {
            res = asyncLogin.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        if (!res.equals("-1")) {
                            Gson gson = new Gson();
                            userLogin = gson.fromJson(res, Usuario.class);
                            if (checkSession.isChecked()) {
                                //Llamar metodo para guardar los datos de la sesion actual en sharedPreferences.
                                saveSession(res);
                            }
                            onLoginSuccess(userLogin);
                            progressDialog.dismiss();
                        } else {
                            onLoginFailed();
                            progressDialog.dismiss();
                        }
                    }
                }, 3000);
    }

    public void onLoginSuccess(Usuario userLogin) {
        loginButton.setEnabled(true);
        Intent i = new Intent(getActivity(), MainActivityS.class);
        i.putExtra("Usuario", userLogin);
        startActivity(i);
    }

    public void onLoginFailed() {
        Toast.makeText(getActivity(), "Autentificación erronea", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = loginEmail.getText().toString();
        String password = loginPass.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginEmail.setError("Introduce una dirección de email valida.");
            valid = false;
        } else {
            loginEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            loginPass.setError("entre 4 y 10 alphanumericos ");
            valid = false;
        } else {
            loginPass.setError(null);
        }

        return valid;
    }

    public void saveSession(String save) {

        SharedPreferences prefs = getActivity().getSharedPreferences("preferencias", getActivity().MODE_PRIVATE);
        SharedPreferences.Editor spEditor = prefs.edit();
        spEditor.putBoolean("recordar", true);
        spEditor.putString("usuario", save);
        spEditor.commit();
    }
}

