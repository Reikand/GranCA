package pantallastutorialandsplash;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.scorp.splashscreen.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Pantallatuto3 extends Fragment {


    public Pantallatuto3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_tres, container, false);
    }


}
