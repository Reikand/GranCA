package pantallastutorialandsplash;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.example.scorp.splashscreen.R;
import loginsingup.LoginActivity;


public class SplashScreen extends Activity {

    // Temporizador para la pantalla de bienvenida
    private static int SPLASH_TIEMPO = 2000;
 //   private  SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        prefs = getSharedPreferences("preferencias", MODE_PRIVATE);

        new Handler().postDelayed(new Runnable() {

			/*
			* Mostramos la pantalla de bienvenida con un temporizador.
			* De esta forma se puede mostrar el logo de la app o
			* compañia durante unos segundos.
			*/

            @Override
            public void run() {
                // Este método se ejecuta cuando se consume el tiempo del temporizador.
                // Se pasa a la activity principal

//                if(prefs.getBoolean("primera", true)){
//
//                    SharedPreferences.Editor spEditor = prefs.edit();
//                    spEditor.putBoolean("primera", false);
//                    spEditor.commit();
//                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
//                    startActivity(i);
//                }else{
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
//                }
                // Cerramos esta activity
                finish();
            }
        }, SPLASH_TIEMPO);
    }
}
