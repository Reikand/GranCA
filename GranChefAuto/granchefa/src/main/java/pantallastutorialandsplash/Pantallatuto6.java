package pantallastutorialandsplash;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.example.scorp.splashscreen.R;

import loginsingup.LoginActivity;


/**
 * A simple {@link Fragment} subclass.
 */
public class Pantallatuto6 extends Fragment {


    public Pantallatuto6() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_image_seis, container, false);
    }

}
