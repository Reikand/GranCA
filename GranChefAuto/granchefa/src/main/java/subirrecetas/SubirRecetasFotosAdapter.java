package subirrecetas;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.scorp.splashscreen.R;

import java.util.List;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class SubirRecetasFotosAdapter extends BaseAdapter {

    List<Bitmap> listaFotos;
    LayoutInflater lf;

    public SubirRecetasFotosAdapter(Context context, List<Bitmap> listaFotos) {
        this.listaFotos = listaFotos;
        this.lf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listaFotos.size();
    }

    @Override
    public Object getItem(int i) {
        return listaFotos.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    static class ViewHolder {
        ImageView image;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = lf.inflate(R.layout.subir_recetas_fotos_item, null);
            holder = new ViewHolder();

            holder.image = (ImageView) convertView.findViewById(R.id.viewFotos);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.image.setImageBitmap(listaFotos.get(i));

        return convertView;
    }
}
