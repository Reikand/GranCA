package subirrecetas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridView;

import com.example.scorp.splashscreen.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class SubirRecetasFotos extends Fragment {

    private static final int CAMERA_REQUEST = 1888;
    private GridView lista;
    public static List<Bitmap> listaFotos = new ArrayList<Bitmap>();
    public static SubirRecetasFotosAdapter adapter;
    public static final String FOTOS_TAG = "SubirRecetasFotos";

    public SubirRecetasFotos() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vSubirFotos = inflater.inflate(R.layout.subirrecetasfotos, container, false);

        hideKeyboard();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.fotos);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
        //Al ser un fragment hay que habilitar el OnCreateMenu.
        setHasOptionsMenu(true);

        lista = (GridView) vSubirFotos.findViewById(R.id.listaFotos);
        SubirRecetasFotosAdapter adapter = new SubirRecetasFotosAdapter(getActivity(), listaFotos);
        lista.setAdapter(adapter);

        return vSubirFotos;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lista_foto, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        //Accedemos a la camara del telefono.
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);


        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case CAMERA_REQUEST:

                if (resultCode == Activity.RESULT_OK) {
                    Bitmap photo = (Bitmap) data.getExtras().get("data");
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                    photo.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    listaFotos.add(photo);
                }
                return;
            default:
                super.onActivityResult(requestCode, resultCode, data);

        }
    }

    public static List<Bitmap> returnListFoto() {
        return listaFotos;
    }


    //Si el teclado está subido lo deshabilita.
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {

        }

    }
}
