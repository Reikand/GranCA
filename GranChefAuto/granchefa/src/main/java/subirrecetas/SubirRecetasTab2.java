package subirrecetas;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.example.scorp.splashscreen.R;

import ListaIngredientes.ListaIngredientes_Fragment;
import floating.MainFloating;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class SubirRecetasTab2 extends Fragment {

    public static final String TAB2_TAG = "SubirRecetasTab2";

    public static ListaIngredientes_Fragment fragmentLista;

    public SubirRecetasTab2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View vSubirTab2 = inflater.inflate(R.layout.subirrecetastab2, container, false);

        hideKeyboard();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.listaFragment);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
        setHasOptionsMenu(true);

        fragmentLista = new ListaIngredientes_Fragment();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.add(R.id.frameList, fragmentLista);
        ft.commit();

        return vSubirTab2;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_lista_ing, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    //Al clicar en el icono del toolbar accedemos al MainFloating.
    public boolean onOptionsItemSelected(MenuItem item) {

        MainFloating floatingFragment = new MainFloating();
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, floatingFragment);
        ft.addToBackStack(null);
        ft.commit();

        return super.onOptionsItemSelected(item);
    }


    //Si el teclado está subido lo deshabilita.
    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {

        }

    }
}
