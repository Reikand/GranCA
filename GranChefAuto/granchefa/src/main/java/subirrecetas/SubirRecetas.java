

package subirrecetas;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import Asyntask.AsyncTaskSubirReceta;
import Constantes.Constantes;
import ListaIngredientes.ListaIngredientes_Fragment;
import POJO.Receta;
import POJO.Usuario;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class SubirRecetas extends Fragment {

    //Objeto usuario de la persona registrada.
    public Usuario user;
    //Componentes del layout de Subir recetas.
    private EditText elaboracion;
    private EditText mReceta;
    private RadioGroup radioGroup;
    private RadioButton radio1, radio2, radio3;
    private EditText mTiempo;
    //Fragment con la Lista de Ingredientes.
    public static SubirRecetasTab2 subirRecetasTab2;
    //Fragment con la Lista de Fotos.
    public static SubirRecetasFotos subirRecetasFotos;


    //Variable con el resultado que recibiremos del servidor
    String res = "";
    String listaImg = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.subirreceta2, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.subirrecetas);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(R.string.subirrecetas2);
        setHasOptionsMenu(true);

        //Recuperamos objeto usuario con toda la informacion que utilizaremos para subir la receta.
        user = (Usuario) getArguments().get("Usuario");

        //Carga de los componentes del Layout.
        mReceta = (EditText) v.findViewById(R.id.nombrereceta);
        radioGroup = (RadioGroup) v.findViewById(R.id.radioGroup);
        radio1 = (RadioButton) v.findViewById(R.id.radioFacil);
        radio2 = (RadioButton) v.findViewById(R.id.radioNormal);
        radio3 = (RadioButton) v.findViewById(R.id.radioDificil);
        elaboracion = (EditText) v.findViewById(R.id.elaboracionreceta);
        mTiempo = (EditText) v.findViewById(R.id.time);

        //Configuracion del componente Spinner al que le añadimos las dificultades guardadas en Strings.xml (PENDIENTE DE CAMBIAR)
//        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(getActivity(), R.array.dificultad, android.R.layout.simple_list_item_1);
//        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinnerDif.setAdapter(adapterSpinner);

        //Creamos el fragment donde se iran añadiendo los ingredientes para que desde un principio se genere
        //la lista y el adaptador y no tener problemas al subir la receta
        subirRecetasTab2 = new SubirRecetasTab2();
        subirRecetasFotos = new SubirRecetasFotos();

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_options_recesubi, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Al pulsar el icono de enviar, se sube la receta al servidor si hay conexion a internet.
        if (id == R.id.action_user) {
                if (validate()) {
                    if (internetConection()) {

                        int id_user = user.getId();
                        String nombre = mReceta.getText().toString();

                        //Dependiendo el radio button marcado se guardara una dificultad diferente.
                        String dificultad = "";
                        if (radio1.isChecked()) {
                            dificultad = radio1.getText().toString();
                        } else if (radio2.isChecked()) {
                            dificultad = radio2.getText().toString();
                        } else if (radio3.isChecked()) {
                            dificultad = radio3.getText().toString();
                        }

                        //Falta comprobar que no esta el campo en blanco.
                        int tiempo = Integer.parseInt(mTiempo.getText().toString());
                        String explicacion = elaboracion.getText().toString();

                        //Recuperamos los ingredientes del ListView, las fotos y generamos el objeto Receta (Sin ingredientes y sin id interna).
                        Map<String, Integer> listaIng = ListaIngredientes_Fragment.returnListIng();
                        //List<Bitmap> listaFotos = SubirRecetasFotos.returnListFoto();
                        Receta receta = new Receta(id_user, nombre, dificultad, tiempo, explicacion, 0, listaIng);

                        //Generamos los 2 objetos en Gson.
                        Gson gsonSubida = new Gson();
                        String rec = gsonSubida.toJson(receta);

                        //Generamos el objeto AsyncTask para que haga las peticiones en segundo plano y lo ejecutamos.
                        AsyncTaskSubirReceta asyncTaskSubirReceta = new AsyncTaskSubirReceta(getActivity());
                        //asyncTaskSubirReceta.execute(url);
                        asyncTaskSubirReceta.execute(Constantes.URL_SUBIR_RECETA, "&receta=", rec);

                        try {
                            res = asyncTaskSubirReceta.get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }

                        if (!res.equals("-1")) {
                            Toast.makeText(getActivity(), "Receta subida correctamente", Toast.LENGTH_SHORT).show();
                            resetScreen();
                        } else {
                            Toast.makeText(getActivity(), "Hemos tenido algun problema al subir la receta", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), "No hay conexión a internet", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Campos vacios", Toast.LENGTH_SHORT).show();
                }

        } else if (id == R.id.ingredientesLista) {

            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, subirRecetasTab2);
            ft.addToBackStack(null);
            ft.commit();

        } else if (id == R.id.fotosLista) {

            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, subirRecetasFotos);
            ft.addToBackStack(null);
            ft.commit();
        }

        return super.onOptionsItemSelected(item);
    }


    //Comprobamos conexión.
    public boolean internetConection() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null);
    }

    public void resetScreen() {

        mReceta.setText("");
        //Cambiar para que no se seleccione ningun radio button
//        spinnerDif.setSelection(0);
        ListaIngredientes_Fragment.listaIngredientes.clear();
        radio1.setChecked(true);
        elaboracion.setText("");
        mTiempo.setText("");
    }

    public boolean validate() {
        boolean valid = true;

        String receta = mReceta.getText().toString();
        String ela = elaboracion.getText().toString();

        //Comprobación de que el formato del email es correcto
        if (receta.isEmpty()) {
            mReceta.setError("Introduce un nombre");
            valid = false;
        } else {
            mReceta.setError(null);
        }

        if (ela.isEmpty()) {
            elaboracion.setError("Introduce la elaboración");
            valid = false;
        } else {
            elaboracion.setError(null);
        }

        return valid;
    }


}



