package BuscarReceta;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.scorp.splashscreen.R;

import java.util.Map;

/**
 * Created by Ruben on 16/05/2016.
 */
public class BuscarRecetaAdapter extends BaseAdapter{

    private Map<String, Integer> listaIngredientes;
    private String[] mKeys;
    private LayoutInflater lf;

    public BuscarRecetaAdapter(Context context, Map<String, Integer> listaIngredientes) {
        this.listaIngredientes = listaIngredientes;
        this.mKeys = listaIngredientes.keySet().toArray(new String[listaIngredientes.size()]);
        lf = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listaIngredientes.size();
    }

    @Override
    public Object getItem(int position) {
        return listaIngredientes.get(mKeys[position]);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes que representen
    static class ViewHolder {

        TextView nombreIngrediente;
        TextView cantidadIngrediente;
        ImageButton btncancel;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {

            convertView = lf.inflate(R.layout.subirrecetasitem2, null);
            holder = new ViewHolder();

            holder.nombreIngrediente = (TextView) convertView.findViewById(R.id.txtIngredientes);
            holder.cantidadIngrediente = (TextView) convertView.findViewById(R.id.txtCantidad);
            holder.btncancel = (ImageButton) convertView.findViewById(R.id.btnCancel);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.nombreIngrediente.setText("" + mKeys[position]);
        holder.cantidadIngrediente.setText("Cantidad: " + getItem(position).toString());
        holder.btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaIngredientes.remove(mKeys[position]);
                mKeys = listaIngredientes.keySet().toArray(new String[listaIngredientes.size()]);
                notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
