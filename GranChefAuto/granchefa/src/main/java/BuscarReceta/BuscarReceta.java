package BuscarReceta;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import Asyntask.AsyncTaskBuscarReceta;
import Constantes.Constantes;
import ListaIngredientes.ListaIngredientes_Fragment;
import POJO.Receta;
import floating.MainFloating;
import resultadolistarecetas.Productselec;


/**
 * A simple {@link Fragment} subclass.
 */
public class BuscarReceta extends Fragment {

    private ImageButton btnAdd;
    public static ListaIngredientes_Fragment fragmentLista;
    String res = "";

    public BuscarReceta() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_buscar_receta, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.buscar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle("");
        setHasOptionsMenu(true);

        fragmentLista = new ListaIngredientes_Fragment();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frameListBuscar, fragmentLista);
//        ft.addToBackStack(null);
        ft.commit();

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_options_buscar_recetas, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.send_buscar_recetas) {
            if (internetConection()) {

                //Recuperamos los ingredientes del ListView y generamos el objeto Receta (Sin ingredientes y sin id interna).
                Map<String, Integer> listaIng = ListaIngredientes_Fragment.returnListIng();

                //Generamos el objeto Gson que enviaremos en la peticion y pasamos el objeto Map a String.
                Gson gson = new Gson();
                String listaIngredientes = gson.toJson(listaIng);

                //Generamos el AsyncTask con los argumentos que enviaremos en la peticion al servidor.
                AsyncTaskBuscarReceta asyncTaskBuscarReceta = new AsyncTaskBuscarReceta(getActivity());
                asyncTaskBuscarReceta.execute(Constantes.URL_BUSCAR_RECETA, "&ingredientes=", listaIngredientes);

                //Recuperamos la informacion que nos retorna el AsycTask.
                try {
                    res = asyncTaskBuscarReceta.get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                //Creamos un bundle con la informacion que enviaremos a la pantalla con la lista de recetas recomendadas.
                Bundle args = new Bundle();
                args.putString("lista", res);

                //Iniciamos la pantalla nueva con las recetas.
                Productselec productselec = new Productselec();
                productselec.setArguments(args);
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragment_container, productselec);
                ft.setCustomAnimations(R.anim.enter_to_right, R.anim.exit_to_left);
                ft.addToBackStack(null);
                ft.commit();

            }

        } else if (id == R.id.addIngredientListBuscar) {

            MainFloating floatingFragment = new MainFloating();
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, floatingFragment);
            ft.setCustomAnimations(R.anim.enter_to_right, R.anim.exit_to_left);
            ft.addToBackStack(null);
            ft.commit();

        }

        return super.onOptionsItemSelected(item);
    }

    public boolean internetConection() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null);
    }
}
