package ListaIngredientes;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.scorp.splashscreen.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class ListaIngredientes_Fragment extends Fragment {

    private ListView listView;

    //Variables que contienen la lista de ingredientes que vamos a subir con la receta.
    public static Map<String, Integer> listaIngredientes = new HashMap<String, Integer>();

    //    Adaptador para el ListView con los ingredientes.
    public static ListaIngredientesAdapter listaAdapter;

    public ListaIngredientes_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vListaIngredientes = inflater.inflate(R.layout.fragment_lista_ingredientes, container, false);

        //Instanciamos el listview que guardara los ingredientes que iremos seleccionando.
        listView = (ListView) vListaIngredientes.findViewById(R.id.listaIngredientesGeneral);

        listaAdapter = new ListaIngredientesAdapter(getActivity(), listaIngredientes);
        listView.setAdapter(listaAdapter);

        return vListaIngredientes;
    }

    public static void addIngredient(String nameIng, int cantidad) {
        listaIngredientes.put(nameIng, cantidad);
    }

    public static boolean containsIngredient(String ingredient) {
        if (listaIngredientes.containsKey(ingredient)) {
            return true;
        }
        return false;
    }

    public static Map<String, Integer> returnListIng() {
        return listaIngredientes;
    }

    public static void removeMap() {
        listaIngredientes.clear();
    }

}
