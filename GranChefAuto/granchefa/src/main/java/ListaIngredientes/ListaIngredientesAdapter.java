package ListaIngredientes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.scorp.splashscreen.R;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class ListaIngredientesAdapter extends BaseAdapter {

    private Map<String, Integer> ingredientes;
    //    private String[] mKeys;
    private List<String> listaKeys;
    private LayoutInflater layoutinflater;

    public ListaIngredientesAdapter(Context context, Map<String, Integer> listaingredientes) {
        this.ingredientes = listaingredientes;
//        this.mKeys = ingredientes.keySet().toArray(new String[listaingredientes.size()]);
        this.listaKeys = Arrays.asList(ingredientes.keySet().toArray(new String[listaingredientes.size()]));
        this.layoutinflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return ingredientes.size();
    }

    @Override
    public Object getItem(int position) {
//        return ingredientes.get(mKeys[position]);
        return ingredientes.get(listaKeys.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes que representen
    static class ViewHolder {

        TextView nombreIngrediente;
        TextView cantidadIngrediente;
        ImageButton btncancel;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

            convertView = layoutinflater.inflate(R.layout.subirrecetasitem2, null);
            holder = new ViewHolder();

            holder.nombreIngrediente = (TextView) convertView.findViewById(R.id.txtIngredientes);
            holder.cantidadIngrediente = (TextView) convertView.findViewById(R.id.txtCantidad);
            holder.btncancel = (ImageButton) convertView.findViewById(R.id.btnCancel);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.nombreIngrediente.setText("" + listaKeys.get(position));
        holder.cantidadIngrediente.setText("Cantidad: " + getItem(position).toString());
        holder.btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ingredientes.remove(listaKeys.get(position));
                //Prueba
                ListaIngredientes_Fragment.listaIngredientes.remove(listaKeys.get(position));
                listaKeys = Arrays.asList(ingredientes.keySet().toArray(new String[ingredientes.size()]));
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public Map<String, Integer> getIngredientes() {
        return ingredientes;
    }

}




