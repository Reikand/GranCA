package resultadolistarecetas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.example.scorp.splashscreen.R;
import java.util.ArrayList;
import java.util.List;
import POJO.Receta;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class CardAdapter extends BaseAdapter implements Filterable {
    private List<Receta> receta;
    private List<Receta> recetaPruebas;
    private LayoutInflater layoutinflater;

    public CardAdapter(Context context, List<Receta> listaRecetas) {
        this.receta = listaRecetas;
        this.layoutinflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return receta.size();
    }

    @Override
    public Object getItem(int position) {
        return receta.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence letra) {
                final FilterResults resultadosFiltrados = new FilterResults();
                final ArrayList<Receta> results = new ArrayList<>();
                if (recetaPruebas == null) {
                    recetaPruebas = receta;
                }
                if (letra != null) {
                    if (recetaPruebas != null && recetaPruebas.size() > 0) {
                        for (final Receta r : recetaPruebas) {
                            //filtramos por el nombre
                            if (r.getNombre().toLowerCase().contains(letra.toString().toLowerCase())) {
                                results.add(r);
                            }
                        }

                    }
                    resultadosFiltrados.values = results;

                } else {
                    //si se pulsa la x, mostrará la lista original
                    resultadosFiltrados.values = recetaPruebas;
                }
                return resultadosFiltrados;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                //creamos lista de resultados temporales
                ArrayList<Receta> listaTemporal = new ArrayList<>();
                //añadimos valores encontrados a la lista
                List<?> result = (List<?>) results.values;
                for (Object object : result) {
                    if (object instanceof Receta) {
                        listaTemporal.add((Receta) object);
                    }
                }
                //asignamos la nueva lista y notificamos a la listview
                receta = listaTemporal;
                notifyDataSetChanged();

            }
        };
    }

    //metodo que notifica cambios en listview
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    static class ViewHolder {
        TextView nombrereceta;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutinflater.inflate(R.layout.itemcard, null);
            holder = new ViewHolder();
            holder.nombrereceta = (TextView) convertView.findViewById(R.id.imagen1);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.nombrereceta.setText(receta.get(position).getNombre());
        return convertView;
    }


}