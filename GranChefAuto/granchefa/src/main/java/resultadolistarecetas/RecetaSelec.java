package resultadolistarecetas;

import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;

import java.util.Iterator;
import java.util.concurrent.ExecutionException;

import POJO.Receta;
import SQLite3Favoritos.Asyntaskfavoritos;
import SQLite3Favoritos.RecetasDBHelper;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class RecetaSelec extends AppCompatActivity {

    boolean flag;
    Receta rec;
    FloatingActionButton floatingActionButton;
    private TextView elabora, time, ingredient, dificult;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receta_selec);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Recuperamos el objeto (item) del listView de las recetas.
        rec = (Receta)getIntent().getExtras().getSerializable("receta");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        elabora = (TextView)findViewById(R.id.elaboracion);
        time = (TextView)findViewById(R.id.tiempo);
        ingredient = (TextView)findViewById(R.id.ingredientestxt);
        dificult = (TextView)findViewById(R.id.dificultad);

        //Añadimos todos los ingredientes que tiene en la receta para que podamos verlos.
        Iterator it = rec.getLista_ingredientes().keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            ingredient.append(key + " - " + rec.getLista_ingredientes().get(key) + "\n");
        }
        elabora.setText(rec.getExplicacion());
        time.setText("" + rec.getTiempo() + " minutos");
        dificult.setText(rec.getDificultad());

        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitle(rec.getNombre());
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        flag = isFavourite(rec);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!flag) {
                    floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_favorite_black));
                    flag = true;
                    addRecetafav(rec);

                } else {
                    floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_favorite));
                    flag = false;
                    deleteRecetafav(rec);

                }
            }

        });
    }

    public void addRecetafav(Receta rec){

        Asyntaskfavoritos af = new Asyntaskfavoritos(this, "insert");
        af.execute(rec);
        String result = "";
        try {
            result = af.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if(result.equals("1")){
            Toast.makeText(RecetaSelec.this, "Añadido", Toast.LENGTH_SHORT).show();
        }
    }

    public void deleteRecetafav(Receta rec){

        Asyntaskfavoritos af = new Asyntaskfavoritos(this, "remove");
        af.execute(rec);
        String result = "";

        try {
            result = af.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if(result.equals("1")){
            Toast.makeText(RecetaSelec.this, "Eliminado", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isFavourite(Receta rec){

        RecetasDBHelper rc = new RecetasDBHelper(this);
        SQLiteDatabase db = rc.getReadableDatabase();

        String query = "SELECT count(*) FROM Favoritos WHERE Nombre='" + rec.getNombre() + "'";

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        if (c.getInt(0) != 0) {
            floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_favorite_black));
            return true;
        } else {
            floatingActionButton.setImageDrawable(getDrawable(R.drawable.ic_favorite));
        }
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_options, menu);

       /* // Associate searchable configuration with the SearchView
        SearchView searchView = (SearchView) findViewById(R.id.search);
        // Sets searchable configuration defined in searchable.xml for this SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));*/


        return true;
    }




}



