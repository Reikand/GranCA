package resultadolistarecetas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import POJO.Receta;


/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class Productselec extends Fragment implements AdapterView.OnItemClickListener {

    private ListView lista;
    private SearchView sv;
    private CardAdapter ca;
    private List<Receta> lr = new ArrayList<Receta>();
    Gson gson = new Gson();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.productselec, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.producto);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle(R.string.producto2);
        setHasOptionsMenu(true);

        //Recuperamos el argumento que pasamos desde el fragment anterior.
        String list = getArguments().getString("lista");

        //Creamos la List parseando el Json que enviamos del anterior fragment.
        Type typeOfList = new TypeToken<List<Receta>>(){}.getType();
        lr = gson.fromJson(list, typeOfList);

        //recuperamos listview y searchview
        lista = (ListView) v.findViewById(R.id.card);
        sv = (SearchView) v.findViewById(R.id.searchView1);
        sv.setVisibility(View.VISIBLE);
        sv.setIconifiedByDefault(false);
        sv.setOnQueryTextListener(escuchadorsv);
        // Que no se muestre al lado de la X el boton de submit
        sv.setSubmitButtonEnabled(false);
        // Para que no aparezca el foco en el filtro de busqueda
        sv.setFocusable(false);
        //asignamos adaptador
        lista.setOnItemClickListener(this);
        ca = new CardAdapter(getContext(), lr);
        lista.setAdapter(ca);

        return v;
    }

    SearchView.OnQueryTextListener escuchadorsv = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            if (ca != null) {
                if (TextUtils.isEmpty(newText)) {
                    ca.getFilter().filter(null);
                } else {
                    ca.getFilter().filter(newText);
                }
            }
            return true;
        }
    };


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Receta re = (Receta) parent.getItemAtPosition(position);

        Intent intent = new Intent(getActivity(), RecetaSelec.class);
        intent.putExtra("receta", re);
        startActivity(intent);

    }


}