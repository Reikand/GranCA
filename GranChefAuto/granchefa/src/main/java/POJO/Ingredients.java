package POJO;

import android.content.res.TypedArray;
import android.media.Image;

import java.util.ArrayList;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class Ingredients {

    private String nombre;
    private String cantidad;


    public Ingredients(String nombre, String cantidad) {
        this.nombre = nombre;
        this.cantidad = cantidad;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
