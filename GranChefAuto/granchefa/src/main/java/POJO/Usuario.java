package POJO;

import java.io.Serializable;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

public class Usuario  implements Serializable{

    private int id;
    private String email;
    private String userName;
    private String password;


    public Usuario(String email, String password) {

        this.email = email;
        this.password = password;
        setUserName(email);
    }

    public Usuario(int id, String email, String userName, String password){
        this.email = email;
        this.password = password;
        this.userName = userName;
        this.id = id;
    }

    public Usuario(int id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String email) {
        String name[] = email.split("@");
        this.userName = name[0];
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Usuario [id=" + id + ", email=" + email + ", userName=" + userName + ", password=" + password + "]";
    }


}
