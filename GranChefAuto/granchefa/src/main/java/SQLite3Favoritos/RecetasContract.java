package SQLite3Favoritos;

import android.provider.BaseColumns;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class RecetasContract {

    private RecetasContract(){

    }

    public static abstract class Recetafavoritos implements BaseColumns{

        public static final String TABLE_NAME = "Favoritos";
        public static final String ID_RECETA = "Id";
        public static final String ID_USUARIO = "Id_user";
        public static final String NOMBRE = "Nombre";
        public static final String RESUMEN = "Resumen";
        public static final String LISTAINGREDIENTES = "Ingredientes";
        public static final String DIFICULTAD = "Dificultad";
        public static final String TIEMPO = "Tiempo";
        public static final String EXPLICACION = "Explicacion";


    }
}
