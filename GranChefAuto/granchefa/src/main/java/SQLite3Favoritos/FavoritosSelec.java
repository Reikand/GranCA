package SQLite3Favoritos;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import POJO.Receta;
import resultadolistarecetas.RecetaSelec;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class FavoritosSelec extends Fragment implements AdapterView.OnItemClickListener {

    private Cursor cursor;
    private FavoritosAdapter favoritoscursor;
    private ListView lista;
    private List<Receta> rece = new ArrayList<Receta>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.favoritos, container, false);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.favoritos);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");

        lista = (ListView) v.findViewById(android.R.id.list);
        lista.setOnItemClickListener(this);
        try {
            consultar();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return v;
    }

    public void consultar() throws SQLException {

        /**
         * Retorna la tabla que nos insteresa de la base de datos con el
         * cursor, en este caso NOMBRE.
         */

        Gson gson = new Gson();
        RecetasDBHelper rdb = new RecetasDBHelper(getActivity());
        SQLiteDatabase db = rdb.getReadableDatabase();
        cursor = rdb.getCursor(db);
        while (cursor.moveToNext()) {

            Type typeOfList = new TypeToken<HashMap<String, Integer>>(){}.getType();
            Map<String, Integer> lista_ingredientes = gson.fromJson(cursor.getString(4), typeOfList);

            Receta re = new Receta(cursor.getInt(1), cursor.getString(2), cursor.getString(5), cursor.getInt(6), cursor.getString(7), cursor.getInt(0), lista_ingredientes);
            rece.add(re);
        }
        //Lo añadimos a la lista de favoritos.
        favoritoscursor = new FavoritosAdapter(getActivity(), rece);
        lista.setAdapter(favoritoscursor);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        /**
         *
         * Iniciamos un intent con los datos de la receta.
         */

        Receta re = (Receta) parent.getItemAtPosition(position);
        Intent i = new Intent(getActivity(), RecetaSelec.class);
        i.putExtra("receta", re);
        startActivity(i);
    }


}
