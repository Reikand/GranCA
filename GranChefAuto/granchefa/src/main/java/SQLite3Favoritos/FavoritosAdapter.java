package SQLite3Favoritos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.scorp.splashscreen.R;

import java.util.List;

import POJO.Receta;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class FavoritosAdapter extends BaseAdapter {
    private List<Receta> fav;
    private LayoutInflater layoutinflater;

    public FavoritosAdapter(Context context, List<Receta> listaFavoritos) {
        this.fav = listaFavoritos;
        this.layoutinflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return fav.size();
    }

    @Override
    public Object getItem(int position) {
        return fav.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Si utilitzem el patró ViewHolder, el primer que farem és crear el model,
    // classe, per als objectes que representen
    static class ViewHolder {

        TextView favoritos;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

            convertView = layoutinflater.inflate(R.layout.fav_item, null);
            holder = new ViewHolder();

            holder.favoritos = (TextView)convertView.findViewById(R.id.textoprueba);


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.favoritos.setText("" + fav.get(position).getNombre());



        return convertView;
    }
}


