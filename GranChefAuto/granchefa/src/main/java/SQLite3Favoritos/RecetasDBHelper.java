package SQLite3Favoritos;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

public class RecetasDBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Favoritos";
    private static final int DATABASE_VERSION = 2;


    private static final String CREATE_TABLE = "create table if not exists " + RecetasContract.Recetafavoritos.TABLE_NAME
            + " (" + RecetasContract.Recetafavoritos.ID_RECETA + " integer primary key, "
            + RecetasContract.Recetafavoritos.ID_USUARIO + " integer, "
            + RecetasContract.Recetafavoritos.NOMBRE + " text, "
            + RecetasContract.Recetafavoritos.RESUMEN + " text, "
            + RecetasContract.Recetafavoritos.LISTAINGREDIENTES + " text, "
            + RecetasContract.Recetafavoritos.DIFICULTAD + " text, "
            + RecetasContract.Recetafavoritos.TIEMPO + " text, "
            + RecetasContract.Recetafavoritos.EXPLICACION + " text)";



    private static final String DELETE_TABLE = " drop table if not exists " + RecetasContract.Recetafavoritos.TABLE_NAME;


    public RecetasDBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion != newVersion){
            db.execSQL(DELETE_TABLE);
            onCreate(db);
        }
    }

    public Cursor getCursor(SQLiteDatabase db) throws SQLException
    {
        //Cursor c = db.query( true, DATABASE_NAME, columnas, null, null, null, null, null, null);
        Cursor c = db.rawQuery("SELECT * FROM Favoritos", null);

        return c;
    }
}
