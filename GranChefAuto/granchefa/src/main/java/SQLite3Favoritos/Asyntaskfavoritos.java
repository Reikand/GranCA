package SQLite3Favoritos;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.google.gson.Gson;

import POJO.Receta;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class Asyntaskfavoritos extends AsyncTask<Receta, Void, String> {

    private Context context;
    private String FLAG;

    public Asyntaskfavoritos(Context context, String action) {
        this.context = context;
        this.FLAG = action;
    }


    @Override
    protected String doInBackground(Receta... receta) {

        RecetasDBHelper rc = new RecetasDBHelper(context);
        SQLiteDatabase db = rc.getReadableDatabase();
        String resultado = "";

        if (FLAG.equals("insert")) {
            if (insertReceta(db, receta[0]) != -1) {
                resultado = "1";
            } else {
                resultado = "-1";
            }
        } else if (FLAG.equals("remove")) {
            if (deleteReceta(db, receta[0]) != 0) {
                resultado = "1";
            } else {
                resultado = "-1";
            }
        }
        return resultado;
    }

    public long insertReceta(SQLiteDatabase db, Receta receta) {

        Gson gson = new Gson();
        ContentValues cv = new ContentValues();
        cv.put(RecetasContract.Recetafavoritos.ID_RECETA, receta.getId());
        cv.put(RecetasContract.Recetafavoritos.ID_USUARIO, receta.getId_user());
        cv.put(RecetasContract.Recetafavoritos.NOMBRE, receta.getNombre());
        cv.put(RecetasContract.Recetafavoritos.LISTAINGREDIENTES, gson.toJson(receta.getLista_ingredientes()));
        cv.put(RecetasContract.Recetafavoritos.DIFICULTAD, receta.getDificultad());
        cv.put(RecetasContract.Recetafavoritos.TIEMPO, receta.getTiempo());
        cv.put(RecetasContract.Recetafavoritos.EXPLICACION, receta.getExplicacion());

        return db.insert(RecetasContract.Recetafavoritos.TABLE_NAME, null, cv);
    }

    public long deleteReceta(SQLiteDatabase db, Receta receta) {
        return db.delete(RecetasContract.Recetafavoritos.TABLE_NAME, RecetasContract.Recetafavoritos.ID_RECETA + "=" + receta.getId(), null);
    }
}


