package floating;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;

import Ingredientes.IngredientsFragment;
import navigation.MainActivityS;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class MainFloating extends Fragment implements View.OnClickListener {

    public Boolean isFabOpen = false;
    private FloatingActionButton fab, fab1, fab2, fab3, fab4, fab5, fab6, fab7, fab8;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
//  private View view;
    private String tipo = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.floating_main, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.floating);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle(R.string.floating2);

        /**
         * Hay veces que, en la pantalla anterior (SubirRecetas.java),
         * si estabamos en un edittext nos aparece el softmenu del teclado.
         * Al cambiar de fragment se mantiene por lo que, con este método abajo
         * indicando, si el teclado estáa true, lo cierra.
         */
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {

        }


        /**
         * Declamos los floatingactionbuttons del floating_main.xml
         * y asignanos los escuchadores.
         */
        fab = (FloatingActionButton) view.findViewById(R.id.fabfloating);
        fab1 = (FloatingActionButton) view.findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) view.findViewById(R.id.fab2);
        fab3 = (FloatingActionButton) view.findViewById(R.id.fab3);
        fab4 = (FloatingActionButton) view.findViewById(R.id.fab4);
        fab5 = (FloatingActionButton) view.findViewById(R.id.fab5);
        fab6 = (FloatingActionButton) view.findViewById(R.id.fab6);
        fab7 = (FloatingActionButton) view.findViewById(R.id.fab7);
        fab8 = (FloatingActionButton) view.findViewById(R.id.fab8);
        fab_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
        fab3.setOnClickListener(this);
        fab4.setOnClickListener(this);
        fab5.setOnClickListener(this);
        fab6.setOnClickListener(this);
        fab7.setOnClickListener(this);
        fab8.setOnClickListener(this);


        return view;
    }


    /**
     * Para cambiar de intents entre fragments
     * hay que hacer un replace del fragment al
     * que vamos ha acceder.
     * Como vamos a utilizarlo en todos los botones
     * del floating creamos una clase genérica que
     * llamamos más abajo.
     */
    public void Fragments() {
        Bundle args = new Bundle();
        args.putString("tipo", tipo);
        Fragment fragment = new IngredientsFragment();
        fragment.setArguments(args);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_to_right, R.anim.exit_to_left);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }


    @Override

    /**
     * Con este método le estamos indicando al boton
     * del floating que layout del gridview
     * de los ingredientes queremos que nos cargue
     * en función del boton seleccionado
     */
    public void onClick(View v) {

        if (v.getId() == R.id.fabfloating) {
            animateFAB();
        } else if (v.getId() == R.id.fab1) {
            tipo = "1";
            Fragments();
        } else if (v.getId() == R.id.fab2) {
            tipo = "2";
            Fragments();
        } else if (v.getId() == R.id.fab3) {
            tipo = "3";
            Fragments();
        } else if (v.getId() == R.id.fab4) {
            tipo = "4";
            Fragments();
        } else if (v.getId() == R.id.fab5) {
            tipo = "5";
            Fragments();
        } else if (v.getId() == R.id.fab6) {
            tipo = "6";
            Fragments();
        } else if (v.getId() == R.id.fab7) {
            tipo = "7";
            Fragments();
        } else if (v.getId() == R.id.fab8) {
            tipo = "8";
            Fragments();
        }
    }


    /**
     * Método utilizado para el boton principal del floating (siempre visible)
     * una vez clicamos en el boton se activa el else (el cual hace rotar la imagen
     * y por consiguiente hace visibles todos los botones.
     * Utliza un false, para que una vez volvamos atras o volvamos a clicar el botón
     * siempre aparezcan los botones (no tenia sentido que pudieran cerrarlos ya que
     * estamos buscando ingredientes).
     */

    public void animateFAB() {

        if (isFabOpen) {
            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab3.startAnimation(fab_close);
            fab4.startAnimation(fab_close);
            fab5.startAnimation(fab_close);
            fab6.startAnimation(fab_close);
            fab7.startAnimation(fab_close);
            fab8.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            fab3.setClickable(false);
            fab4.setClickable(false);
            fab5.setClickable(false);
            fab6.setClickable(false);
            fab7.setClickable(false);
            fab8.setClickable(false);
            isFabOpen = false;

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab3.startAnimation(fab_open);
            fab4.startAnimation(fab_open);
            fab5.startAnimation(fab_open);
            fab6.startAnimation(fab_open);
            fab7.startAnimation(fab_open);
            fab8.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            fab3.setClickable(true);
            fab4.setClickable(true);
            fab5.setClickable(true);
            fab6.setClickable(true);
            fab7.setClickable(true);
            fab8.setClickable(true);
            isFabOpen = false;
        }
    }


}



