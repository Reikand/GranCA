package Constantes;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class Constantes {

    public static final String URL_REGISTRO = "http://gcaservlet-granchefauto.rhcloud.com/ServletRegistroUsuario?";
    public static final String URL_LOGIN = "http://gcaservlet-granchefauto.rhcloud.com/ServletLogin?";
    public static final String URL_SUBIR_RECETA = "http://gcaservlet-granchefauto.rhcloud.com/ServletIntroducirReceta?";
    public static final String URL_BUSCAR_RECETA = "http://gcaservlet-granchefauto.rhcloud.com/ServletBuscarReceta?";
    public static final String URL_SUBIR_RECETA_LOCAL = "http://192.168.1.33:8080/ServletGCA/ServletIntroducirReceta?";
    public static final String URL_BUSCAR_RECETA_LOCAL = "http://192.168.1.33:8080/ServletGCA/ServletBuscarReceta?";
    public static final String URL_REGISTRO_LOCAL = "http://192.168.1.33:8080/ServletGCA/ServletRegistroUsuario?";
    public static final String URL_LOGIN_LOCAL = "http://192.168.1.33:8080/ServletGCA/ServletLogin?";

}
