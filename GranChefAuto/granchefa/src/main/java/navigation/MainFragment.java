package navigation;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.scorp.splashscreen.R;
import POJO.Usuario;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private Usuario user;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.blank, container, false);

        user = (Usuario) getArguments().getSerializable("Usuario");

        //Indicamos el usuario en el toolbar.

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(user.getUserName());
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle("");

        return rootView;
    }


}
