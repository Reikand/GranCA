package navigation;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;
import BuscarReceta.BuscarReceta;
import POJO.Usuario;
import SQLite3Favoritos.FavoritosSelec;
import loginsingup.LoginActivity;
import subirrecetas.SubirRecetas;

public class MainActivityS extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    NavigationView navigationView;
    Toolbar toolbar = null;
    TextView nUser;
    TextView emailText;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_s);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //RECUPERAR CUANDO SE NECESITE DEL SHARED PREFERENCES.
        usuario = getsavedSessio();

        //Cargamos el Mainfragment.
        Bundle args = new Bundle();
        args.putSerializable("Usuario", usuario);
        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.nav_view);

        //Para cambiar las opciones del navigator.
        View headerView = navigationView.getHeaderView(0);
        //Nos dimos cuenta que sin esto, las versiones anteriores a la 23 se ralentizaba el menu.
        //Con esta opción quitamos el acelerador de hardware.
        headerView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);


        nUser = (TextView) headerView.findViewById(R.id.username);
        emailText = (TextView) headerView.findViewById(R.id.email);

        //Indicamos el usuario y el email del usuario en el navigationdrawer
        if (!usuario.getUserName().equals("Invitado")) {
            navigationView.inflateMenu(R.menu.activity_main_drawer);
            nUser.setText("Bienvenido, " + usuario.getUserName());
            emailText.setText(usuario.getEmail());
        } else {
            navigationView.inflateMenu(R.menu.activity_main_drawer2);
            nUser.setText("Bienvenido, " + usuario.getUserName());
        }
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }
    /**
     * Si clicamos el backbutton cerraremos el navigationdrawer pero no
     * saldremos de la aplicación.
     */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /**
         * Acciones del escuchador los botones del NavigationDrawer
         */
        if (id == R.id.buscarReceta) {

            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            BuscarReceta fragment = new BuscarReceta();
            ft.setCustomAnimations(R.anim.enter_to_right, R.anim.exit_to_left);
            ft.replace(R.id.fragment_container, fragment);
            ft.addToBackStack(null);
            ft.commit();

        } else if (id == R.id.fav) {

            FavoritosSelec fragment = new FavoritosSelec();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_to_right, R.anim.exit_to_left);
            ft.replace(R.id.fragment_container,fragment);
            ft.addToBackStack(null);
            ft.commit();

        }else if (id == R.id.subirReceta) {

            /**
             * Para poder pasar entre intents y fragments el objeto usuario,
             * habia que serializarlo.
             */
            Bundle args = new Bundle();
            args.putSerializable("Usuario", usuario);
            SubirRecetas subirFragment = new SubirRecetas();
            subirFragment.setArguments(args);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.enter_to_right, R.anim.exit_to_left);
            ft.replace(R.id.fragment_container, subirFragment);
            ft.addToBackStack(null);
            ft.commit();

        } else if (id == R.id.session) {

            //Cerramos sesion y borramos los datos del sharedpreferences.

            SharedPreferences preferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("recordar", false);
            editor.commit();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Usuario getsavedSessio(){
        SharedPreferences prefs = getSharedPreferences("preferencias", MODE_PRIVATE);
        String userString = prefs.getString("usuario", null);
        Gson gson = new Gson();
        return gson.fromJson(userString, Usuario.class);
    }
}

