package loginsingup;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import Asyntask.AsyncTaskRegistro;
import Constantes.Constantes;
import POJO.Usuario;
import butterknife.ButterKnife;
import butterknife.InjectView;
import navigation.MainActivityS;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    @InjectView(R.id.input_email)
    EditText emailText;
    @InjectView(R.id.input_password)
    EditText passwordText;
    @InjectView(R.id.btn_signup)
    Button signupButton;
    @InjectView(R.id.link_login)
    TextView loginLink;
    ProgressDialog progressDialog;
    String res = "";
    Usuario user;
    Gson gson = new Gson();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.inject(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finalizamos la pantalla de registro y volvemos al login.
                finish();
            }
        });
    }

    public void signup() {
        Log.d(TAG, "Registrado");

        signupButton.setEnabled(true);

        if (!validate()) {
            onSignupFailed();
            return;
        } else {
            progressDialog = new ProgressDialog(SignupActivity.this,
                    R.style.AppTheme);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creando cuenta...");
            progressDialog.show();

            String email = emailText.getText().toString();
            String password = passwordText.getText().toString();

            user = new Usuario(email, password);

            String objectgson = gson.toJson(user);
            String url = Constantes.URL_REGISTRO + "&Json=" + objectgson;

            AsyncTaskRegistro registro = new AsyncTaskRegistro(this);
            registro.execute(url);

            try {
                res = registro.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }


        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // Dependiendo del valor que devuelva el signup
                        if (!res.equals("-1")) {
                            onSignupSuccess(res);
                            progressDialog.dismiss();
                            finish();
                        } else {
                            onSignupFailed();
                            progressDialog.dismiss();
                        }
                    }
                }, 3000);
    }


    public void onSignupSuccess(String res) {
        signupButton.setEnabled(true);
        onlyActualSession(res);
        Intent i = new Intent(this, MainActivityS.class);
//        i.putExtra("Usuario", user);
        startActivity(i);
    }

    /**
     * Guardamos en el SharedPreferences que este telefono ya ha
     * accedido y ha activado el boton de recordar, para entrar
     * automaticamente al abrirla.
     */

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Email ya registrado en la base de datos", Toast.LENGTH_LONG).show();
        signupButton.setEnabled(true);
    }


    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        //Comprobación de que el formato del email es correcto
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Introduce una dirección de email valida.");
            valid = false;
        } else {
            emailText.setError(null);
        }
        //Contraseña entre 4 y 10 caracteres.
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("Entre 4 y 10 carácteres.");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    /**
     * Realmente con este metodo haremos que entre automaticamente al ser correcto el registro.
     */

    public void onlyActualSession(String save) {
        SharedPreferences prefs = getSharedPreferences("preferencias", MODE_PRIVATE);
        SharedPreferences.Editor spEditor = prefs.edit();
        spEditor.putString("usuario", save);
        spEditor.commit();
    }

}
