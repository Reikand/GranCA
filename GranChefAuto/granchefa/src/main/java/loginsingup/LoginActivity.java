package loginsingup;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.scorp.splashscreen.R;
import com.google.gson.Gson;

import java.util.concurrent.ExecutionException;

import Asyntask.AsyncTaskLogin;
import Constantes.Constantes;
import POJO.Usuario;
import butterknife.ButterKnife;
import butterknife.InjectView;
import navigation.MainActivityS;

/**
 * Created by Ruben Martinez.
 * Created by Ruben Suñé.
 * Created by Raúl Nayach.
 */
public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @InjectView(R.id.input_email)
    EditText emailText;
    @InjectView(R.id.input_password)
    EditText passwordText;
    @InjectView(R.id.btn_login)
    Button loginButton;
    @InjectView(R.id.link_signup)
    TextView signupLink;
    @InjectView(R.id.link_invitado)
    TextView linkinvitado;
    @InjectView(R.id.checkbox)
    CheckBox checkSession;
    ProgressDialog progressDialog;
    Usuario userLogin;
    String res = "";
    Gson gson = new Gson();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autentificando...");

        SharedPreferences prefs = getSharedPreferences("preferencias", MODE_PRIVATE);

        if (prefs.getBoolean("recordar", false)) {
            //Recuperamos el usuario guardado en las sharedPreferences e iniciamos la sesion.
            String savedUser = prefs.getString("usuario", "");
            savedUser.toString();
            userLogin = gson.fromJson(savedUser, Usuario.class);
            emailText.setText(userLogin.getEmail());
            passwordText.setText(userLogin.getPassword());
            checkSession.setChecked(true);


            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            progressDialog.show();
                            onLoginSuccess();
                            progressDialog.dismiss();
                            finish();
                        }
                    }, 3000);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hacemos un intent a la ventana de registrarse.
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });

        linkinvitado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Iniciamos la aplicacion con el usuario Invitado.
                Usuario user = new Usuario(0, "Invitado");
                Gson gson = new Gson();
                String usuarioInv = gson.toJson(user);
                onlyActualSession(usuarioInv);
                Intent i = new Intent(getApplicationContext(), MainActivityS.class);
                startActivity(i);
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        loginButton.setEnabled(true);
        progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Autentificando...");
        progressDialog.show();

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        String url = Constantes.URL_LOGIN + "&email=" + email + "&pass=" + password;

        AsyncTaskLogin asyncLogin = new AsyncTaskLogin(this);

        asyncLogin.execute(url);

        try {
            res = asyncLogin.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // Dependiendo del valor que devuelva el login.
                        if (!res.equals("-1")) {
                            if (checkSession.isChecked()) {
                                //Llamar metodo para guardar los datos de la sesion actual en sharedPreferences.
                                saveSession(res);
                            } else {
                                onlyActualSession(res);
                            }
                            onLoginSuccess();
                            progressDialog.dismiss();
                            finish();
                        } else {
                            onLoginFailed();
                            progressDialog.dismiss();
                        }
                    }
                }, 3000);
    }

    @Override
    public void onBackPressed() {
        // Desahbilita volver al MainActivity
        moveTaskToBack(true);
    }

    //En caso de ser exitoso el login acedemos al mainactivity
    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        setResult(RESULT_OK, null);
        Intent i = new Intent(this, MainActivityS.class);
        startActivity(i);
    }

    //En caso de ser negativo el login, mostramos un toast.
    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Autentificación erronea", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();

        //Comprobación de que el formato del email es correcto
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            emailText.setError("Introduce una dirección de email valida.");
            valid = false;
        } else {
            emailText.setError(null);
        }

        //Contraseña entre 4 y 10 caracteres.
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError("entre 4 y 10 carácteres ");
            valid = false;
        } else {
            passwordText.setError(null);
        }

        return valid;
    }

    /**
     * Guardamos en el SharedPreferences que este telefono ya ha
     * accedido y ha activado el boton de recordar, para entrar
     * automaticamente al abrirla.
     */

    public void saveSession(String save) {

        SharedPreferences prefs = getSharedPreferences("preferencias", MODE_PRIVATE);
        SharedPreferences.Editor spEditor = prefs.edit();
        spEditor.putBoolean("recordar", true);
        spEditor.putString("usuario", save);
        spEditor.commit();

    }


    /**
     * Guardamos en el SharedPreferences que este telefono ya ha
     * accedido y como no ha activado el boton de recordar, no le
     * guardará los usuarios al entrar de nuevo.
     */

    public void onlyActualSession(String save){
        SharedPreferences prefs = getSharedPreferences("preferencias", MODE_PRIVATE);
        SharedPreferences.Editor spEditor = prefs.edit();
        spEditor.putString("usuario", save);
        spEditor.commit();
    }
}